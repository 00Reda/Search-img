import { Injectable } from "@angular/core";

// the role of the services : send server requets to add an item or to recieve data

// https://stackoverflow.com/questions/38479667/import-crypto-js-in-an-angular-2-project-created-with-angular-cli

@Injectable()
export class AboutService{

    info  = {
        name : "rida htiti",
        email:" htiti@gmail.com",
        tel: 1214529496,
 
    };
 
    comments=[
         {
           date: new Date(),
           msg: " hi "
 
         },
         {
           date: new Date(),
           msg: " salam"
 
         },
         {
           date: new Date(),
           msg: " hello "
 
         }
          
 
    ];
 
   

    addComment(c){
        this.comments.push(c);
    }

    getAllComments(){
        return this.comments;
    }

    getInfo(){
        return this.info;
    }
}