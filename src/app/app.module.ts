import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule } from "@angular/forms"
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { ContactsComponent } from './contacts/contacts.component';
import { AboutService } from '../services/about.service';


// les decorateur equivalant au anotation en java 
// @ngmodule / role : 
// un composent se base sur 4 fichiers : fich.html : partie html du composont 
@NgModule({
  declarations: [
    // a chaque fois que je cree un composont web il faut le declarer ici 
    AppComponent,
    AboutComponent,
    ContactsComponent
  ],
  imports: [

    // in order to use another module in this one we need to import it her
    BrowserModule,
    FormsModule
  ],

  // to use a service we need to declare it her 
  providers: [AboutService],
  // her we specify the root component 
  bootstrap: [AppComponent]
})
export class AppModule { }
