import { Component, OnInit } from '@angular/core';
import { AboutService } from '../../services/about.service';

@Component({
  selector: '[app-about]',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

   // by default if we didn't specify the type of a var in typeScript it gonna be type any 
   // in order to format data we use pipes
/*
  // we will declare those vars in the srvice 
   info  = {
       name : "rida htiti",
       email:" htiti@gmail.com",
       tel: 1214529496,

   };

   comments=[
        {
          date: new Date(),
          msg: " hi "

        },
        {
          date: new Date(),
          msg: " salam"

        },
        {
          date: new Date(),
          msg: " hello "

        }
         

   ];

   
*/

/*
    constructor(private aboutService : AboutService) { } 
    == 
    private aboutService : AboutService ;
    constructor( aboutService : AboutService) { 
      this.aboutService=aboutService;
    }

**/
  
   info: {name: string , email: string , tel: number }  ;
   comments=[] ; 
   commantaire = {date : null , msg :""} ;
    
  constructor(private aboutService : AboutService) {
    this.info=this.aboutService.getInfo();
    this.comments=this.aboutService.getAllComments();
   }

  ngOnInit() {
  }
  /*
   // first way to manage forms 
  onAddCommantaire(){
    this.commantaire.date=new Date();
    this.comments.push(this.commantaire);
    this.commantaire={date: null , msg:""}
  }

  */
 
  /*
  onAddCommantaire(c ){
    console.log(c)
    this.commantaire.date=new Date();
    this.comments.push({date: new Date() , msg:c.message});
    this.commantaire.msg="";
    
  }
  */
  // using a srvice :

  onAddCommantaire(c ){
    
    this.commantaire.date=new Date();
    this.aboutService.addComment({date: new Date() , msg:c.message});
    this.commantaire.msg="";
    this.comments=this.aboutService.getAllComments();
    
  }

}
