import { Component } from '@angular/core';


// her we user the @component decorator
@Component({
  // selector is the name of the tag that we will user to insert this component in the html part 

  //selector: '.app-root', // to insert it as a css class
  selector: 'app-root',
  templateUrl: './app.component.html', // the html part of the component : either a extern file or her
  styleUrls: ['./app.component.css'] // the css part of the componant : either a extern file or her 
})
export class AppComponent {
  title = 'app';
  contact= {
    name : 'rida',
    email  :"rida@gmail.com"
  }
}
